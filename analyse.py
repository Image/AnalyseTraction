#This code permits to analyse data coming out of traction/compression experiments to get  displacement field,
#Young modulus and Poisson ratio in the case of 'large' deformations or for experiments where the pattern
#change

#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Load libraries:

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import RectangleSelector
from os import system
from glob import glob
from datetime import datetime
from PIL import Image
import time
from random import uniform
from scipy.optimize import minimize
from scipy import ndimage as nd
from multiprocessing import Pool, Array
from math import sqrt, pi
from scipy.interpolate import griddata

#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Input:

## Data location and properties:
### Folder with pictures:
name_dir_im='picture'
### Picture name:
name_im='T40-0,4-T-L-1_' 
### Picture extension:
name_ext_im='.bmp'
### Name of the reference picture for pixel qualibration:
name_ref='picture/'+name_im+'0000'+name_ext_im
### Distance to report on reference picture (mm):
dist_ref=12.5
### Number of digit in the image numbering:
nb_dgt_im=4

### Sample section (m^2):
smpl_sec=(0.4*0.001)*(12.5*0.001)

### File with image information (left if not applicable):
name_im_info='data_img.txt'
### File with force information (left if not applicable):
name_force='data_force.txt'

## Image correlation inputs:
### Maximum number of pixel per cells in line and row [px]:  
cc_0_M=80
### Minimum number of pixel per cells in line and row [px]:  
cc_0_m=40
### Maximum shift optimization (for optimization algorithm) [px<<cc_0_m]:
m_sht=3
### Number of cell size subdivision :
nc=2
### Space between cell centers in line and row [px]:
dd_0=40

## Hardware inputs:
### Number of processor for parallelization:
nb_proc=6

## Experiment nature:
### Global stain computation (True or False):
E_cmpt=1
### Young modulus computation (True or False):
Y_cmpt=1
### Poisson ratio computation (True or False, if True Y_cmpt must be True):
N_cmpt=1
### Plastic modulus computation (True or False, if True Y_cmpt must be True):
PM_cmpt=1
### Maximum stress computation (True or False, if True Y_cmpt must be True):
sigM_cmpt=1
### Last stress computation (True or False, if True Y_cmpt must be True):
sigL_cmpt=1
### elastic plastic transtion stress computation (True or False, if True Y_cmpt must be True):
sigT_cmpt=1
### Large (True) or small (False) deformation assumption:
Lrg_Sml=1
### Strain tensor computation:
strain_cmpt=1

## Displaying choice:
### Number of images per second for movies (integer):
n_fps=20
### Raw displacement vs. force and time of picture (1/0):
grph_1=1
### Raw image movie (1/0):
grph_2=1
### DIC arrow movie (1/0):
grph_3=0
### DIC cell dimension control plot (1/0):
grph_4=1
### DIC graphical output (1/0):
grph_5=1
### Mechanical analysis (1/0):
grph_6=1

## Vector of the images to consider:
### Number of the first image to treat:
nb_img_0=0
### Number of images:
nb_img=len(glob(name_dir_im+'/*'+name_ext_im))
### IMage number vector:
vec_img=np.hstack((np.arange(nb_img_0,int(0.15*nb_img),1),np.arange(int(0.15*nb_img),int(0.9*nb_img),6),np.arange(int(0.9*nb_img),nb_img-1,1)))
# ~ vec_img=np.arange(nb_img_0,nb_img-1,1)


#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Get force, time from loading device:

if Y_cmpt:
    ## Read the text file:
    ### Read all the line:
    with open(name_force) as f:
        dat_line=f.readlines()[7:]
    
    ### Make storage variables:
    vec_f=np.zeros(len(dat_line)-2); vec_d=np.zeros(len(dat_line)-2); vec_t=np.zeros(len(dat_line)-2)
    ### Extract data from lines:
    for it_ln in range(len(dat_line)-2):
        cur_lin=dat_line[it_ln]
        I=([pos for pos, char in enumerate(cur_lin) if char==','])
        vec_f[it_ln]=float(cur_lin[0:I[0]])
        vec_t[it_ln]=float(cur_lin[I[0]+1:I[1]])
        vec_d[it_ln]=-float(cur_lin[I[1]+1:I[2]])/1000.
    
    ## Make sure time increases:
    I=np.argsort(vec_t)
    vec_t=vec_t[I][1:]
    vec_f=vec_f[I][1:]
    vec_d=vec_d[I][1:]
    
    ## Save data:
    system('mkdir data')
    np.savetxt('data/direct_time_s.txt',vec_t)
    np.savetxt('data/direct_force_N.txt',vec_f)
    np.savetxt('data/direct_displacement_m.txt',vec_d)



#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Get time for images and prepare image variables:

if Y_cmpt:
    ## Read the text file:
    ### Read all the line:
    with open(name_im_info) as f:
        dat_line=f.readlines()
    
    ### Make storage variables:
    vec_t_im=np.zeros(len(dat_line))
    ### Extract data from lines:
    for it_ln in range(len(dat_line)):
        cur_lin=dat_line[it_ln]
        I=([pos for pos, char in enumerate(cur_lin) if char==':'])
        ms0=float(cur_lin[I[-1]+1:-1])
        s0=float(cur_lin[I[-2]+1:I[-1]])
        m0=float(cur_lin[I[-3]+1:I[-2]])
        h0=float(cur_lin[I[-3]-2:I[-3]])
        vec_t_im[it_ln]=h0*3600.+m0*60+s0+ms0/1000.
    
    ## Correct vector length:
    vec_t_im=vec_t_im[0:nb_img]
    
    ## Save data:
    np.savetxt('data/image_time_s.txt',vec_t_im)
    
    ## Display:
    if grph_1:
        ### Make storage folder
        system('mkdir figure')
        ### Interpolate force and displacement on image time:
        f_tmp=np.interp(vec_t_im,vec_t,vec_f)
        d_tmp=np.interp(vec_t_im,vec_t,vec_d)
        ### Display:
        plt.plot(vec_d,vec_f,'-k',linewidth=2)
        plt.plot(d_tmp,f_tmp,'bx',markersize=5)
        plt.xlabel('direct displacement (m)')
        plt.ylabel('direct force (N)')
        plt.savefig('figure/direct_force_displacement.png')
        plt.close()

## Make deformatiom movie from pictures:
if grph_2:
    ### Make storage folder:
    system('mkdir figure')
    ### Make movie:
    system("ffmpeg -y -r "+str(n_fps)+" -f image2 -i "+name_dir_im+"/"+name_im+"%0"+str(nb_dgt_im)+"d"+name_ext_im+" -qscale 20 figure/img_vid.avi")


#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Make pixel calibration:

## Load undeformed picture picture:
pict_0=Image.open(name_ref)

## Select sample width:
def onclick(event):
    global x0, y0
    if event.button==3 and event.inaxes:
        x0.append(event.xdata)
        y0.append(event.ydata)
        plt.plot(event.xdata,event.ydata,'b+')
        plt.draw() 

fig,ax=plt.subplots()
ax.imshow(pict_0)
ax.set_title('Click on point separated of '+str(int(dist_ref))+'mm\n -right click to select, left click to zoom, close to end-')
x0=[]; y0=[]
cid=fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()
plt.close()

## Compute the conversion between pixel and m and save it:
system('mkdir data')
px2m=sqrt((x0[0]-x0[1])**2.+(y0[0]-y0[1])**2.)/(dist_ref/1000.)
np.savetxt('data/pixel2meter.txt',np.array([px2m]))


#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Select sample ROI:

## Load undeformed picture picture:
pict_0=Image.open(name_dir_im+'/'+name_im+str(0).zfill(nb_dgt_im)+name_ext_im)

## Make functions to define a rectangle:
def line_select_callback(eclick, erelease):
    global x1, x2, y1, y2
    x1,y1=eclick.xdata,eclick.ydata
    x2,y2=erelease.xdata,erelease.ydata

def toggle_selector(event):
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        toggle_selector.RS.set_active(True)

## Click to select the ROI: 
fig,ax=plt.subplots()
plt.imshow(pict_0)
plt.title('clic and drag to select a ROI, close to end')
toggle_selector.RS=RectangleSelector(ax,line_select_callback,drawtype='box',useblit=True,button=[1, 3],minspanx=5,minspany=5,spancoords='pixels',interactive=True)
plt.connect('key_press_event', toggle_selector)
plt.show()

## Store ROI:
xm=min(x1,x2)
xM=max(x1,x2)
ym=min(y1,y2)
yM=max(y1,y2)


#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Perform DIC:

########################
# Initialization:

## Make graphical output storage folder:
if grph_3:
    system('mkdir figure')
    system('mkdir figure/tmp_arrow')

## Make indice vector for each correlation cell:
### Get image size:
image_tmp=nd.imread(name_dir_im+'/'+name_im+str(nb_img_0).zfill(nb_dgt_im)+name_ext_im,flatten=1)
S00=image_tmp.shape
Si=S00[0]
Sj=S00[1]
### Compute the number of cells:
nbi_0=Si/dd_0
nbj_0=Sj/dd_0
### Make cell size vector:
vec_cc=np.linspace(cc_0_M,cc_0_m,nc).astype(int)
### Make cell postion vectors:
vec_i0=np.linspace(0,Si,nbi_0).astype(int)
vec_j0=np.linspace(0,Sj,nbj_0).astype(int)
mat_i, mat_j=np.meshgrid(vec_i0,vec_j0) 
vec_i0=np.reshape(mat_i,[mat_i.size])
vec_j0=np.reshape(mat_j,[mat_j.size])

## Reduction inside the ROI:
### Initilalise storagre vectors:
vec_iN=[]
vec_jN=[]
### Loop over the cell sizes :
val_test_th=np.mean(image_tmp)*1.1
for it in range(len(vec_i0)):
    icc=vec_i0[it]
    jcc=vec_j0[it]
    if ((icc>ym) & (icc<yM) & (jcc>xm) & (jcc<xM)): 
        vec_iN.append(icc)
        vec_jN.append(jcc)

vec_i0=vec_iN
vec_j0=vec_jN
del vec_iN, vec_jN

if grph_4:
    plt.figure(num=None,figsize=(10,10),dpi=100,facecolor='w',edgecolor='k')
    plt.imshow(image_tmp,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
    plt.colorbar()
    cmap=plt.cm.jet
    for it in range(len(vec_i0)):
        col=cmap(uniform(0.,1.))
        i0=vec_i0[it]
        j0=vec_j0[it]
        plt.plot(j0,i0,'o',color=col,markersize=5)
        if it<10:
            im=(i0-cc_0_m/2.); iM=(i0+cc_0_m/2.)
            jm=(j0-cc_0_m/2.); jM=(j0+cc_0_m/2.)
            plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-',color=col,linewidth=2)
            im=(i0-cc_0_M/2.); iM=(i0+cc_0_M/2.)
            jm=(j0-cc_0_M/2.); jM=(j0+cc_0_M/2.)
            plt.plot([jm,jm,jM,jM,jm],[im,iM,iM,im,im],'-',color=col,linewidth=2)
            plt.text(j0,i0,str(it))
    
    plt.show()
    plt.close()


########################
# Define function to measure image correlation, for subpixel accurate displacement measurement:
# This function measures the goodness of correlation in the area zone between image In and I0 deformed by a X[1] X[2] translation:   

def translation_correlation_weight(X,C0,C1,m_sht0):
    """
    Correlation measurement function for a given translation 
    - inputs:
    -- X: translation vector (line, row)
    -- C0: initial picture
    -- C1: deformed picture
    -- m_sht: maxmum absolute values for X
    - output:
    -- s: goodness of correlation between undeformed and shifted picture 
    """  
    ## Extraction of the variables:
    dI=X[0]
    dJ=X[1]
    ##Condition on the maximum shift:
    if (np.max(X)>m_sht):
        ###Make increasing s:
        s=255.+np.max(X)
    else:
        ## Shift the picture:
        C1=nd.interpolation.shift(C1,(dI,dJ),order=1,mode='constant',cval=0.,prefilter=False)
        ## Reduction to the center of the ROI:
        dd=len(C0)
        C0=C0[m_sht:dd-m_sht,m_sht:dd-m_sht]
        C1=C1[m_sht:dd-m_sht,m_sht:dd-m_sht]
        ## Correlation:
        s=np.sum(abs(C1-C0))/(dd**2)
    
    return s


########################
# Define function for parallelisation:

## Define share variable:
share_vec=Array('d',np.zeros(3*len(vec_i0)))

## Define main DIC function:
def main(itCell,def_param=share_vec):
    #### Global variables:
    global it_vec, vec_img, mat_di, mat_dj, mat_corr, ve_cc, I0, I1, nb_img_1, nb_img_0, Lrg_Sml
    
    #### Display:
    print('   '+str(it_vec)+'/'+str(len(vec_img))+' - '+str(itCell)+'/'+str(len(vec_i0)-1))
    
    #### Get current cell displacement:
    di_cur=mat_di[itCell,it_vec]
    dj_cur=mat_dj[itCell,it_vec]
    
    if not(((vec_i0[itCell]+di_cur)<0) or ((vec_i0[itCell]+di_cur)>Si) or ((vec_j0[itCell]+dj_cur)<0) or ((vec_j0[itCell]+dj_cur)>Sj)):
        
        ####Initialize vector:
        vec_corr=np.zeros(len(vec_cc))
        vec_p0=np.zeros((len(vec_cc),2))
        p0=np.zeros(2)
        
        #### Loop over the decreasing correlation cell sizes for FFT shift measurement:
        for itSiz in range(len(vec_cc)):
            ##### Construction of the zone pixel coordinates:
            im=int(vec_i0[itCell]-vec_cc[itSiz]/2.); iM=int(vec_i0[itCell]+vec_cc[itSiz]/2.)
            jm=int(vec_j0[itCell]-vec_cc[itSiz]/2.); jM=int(vec_j0[itCell]+vec_cc[itSiz]/2.)
            ###### Make coordinate vectors: 
            vec_i=range(im,iM)
            vec_j=range(jm,jM)
            ###### Make mesh and store it in a vector:
            mat_i,mat_j=np.meshgrid(vec_i,vec_j) 
            ze_X=np.empty([mat_i.size,2])
            ze_X[:,0]=np.reshape(mat_i,[mat_i.size])
            ze_X[:,1]=np.reshape(mat_j,[mat_j.size])
            ###### Zone for current image:
            if Lrg_Sml:
                ze_0=ze_X+[di_cur,dj_cur]
            else:
                ze_0=ze_X
            
            ###### Zone for next image:
            ze_1=ze_X+[di_cur,dj_cur]-p0[::-1]
            ##### Load picture cells:
            ###### Current/undeformed picture (as a vector):
            cell00=nd.map_coordinates(I0,ze_0.T,order=1) 
            ###### Next one with the correction of the previously measured displacement (as a vector):
            cell01=nd.map_coordinates(I1,ze_1.T,order=1) 
            ###### Change it into a 2D picture:
            cell0=np.reshape(cell00,(int(sqrt(len(cell00))),int(sqrt(len(cell00))))).T
            cell1=np.reshape(cell01,(int(sqrt(len(cell00))),int(sqrt(len(cell00))))).T
            ##### Define image size: 
            row,col=cell0.shape
            ##### Measure shift between picture from FFT:
            ###### Compute Fourier transform:
            cell0FFT=np.fft.fft2(cell0)
            cell1FFT=np.conjugate(np.fft.fft2(cell1))
            ###### Convolute:
            pictCCor=np.real(np.fft.ifft2((cell0FFT*cell1FFT)))
            ###### Compute the shift: 
            pictCCorShift=np.fft.fftshift(pictCCor)
            iShift,jShift=np.unravel_index(np.argmax(pictCCorShift),(row,col))
            iShift=iShift-int(row/2)+p0[1]
            jShift=jShift-int(col/2)+p0[0]
            ##### Store results:
            p0[0]=jShift
            p0[1]=iShift
            vec_p0[itSiz,0]=p0[0]
            vec_p0[itSiz,1]=p0[1]
            
            ##### Compute the correlation goodness:
            ###### Compute the shifted next picture:
            ze_1=ze_X+[di_cur,di_cur]-p0[::-1]
            cell01=nd.map_coordinates(I1,ze_1.T,order=1)
            cell1=np.reshape(cell01,(int(sqrt(len(cell01))),int(sqrt(len(cell01))))).T
            ###### Compute the correlation and store it:
            vec_corr[itSiz]=np.sum(np.abs(cell0-cell1))/vec_cc[itSiz]**2.
        
        #### Subpixel improvement of the displacement:
        ##### Look for the best correlation:
        itSiz_0=np.where(vec_corr==np.min(vec_corr))[0][0]
        ##### Extract current and shifted next pictures:
        im=int(vec_i0[itCell]-vec_cc[itSiz_0]/2.); iM=int(vec_i0[itCell]+vec_cc[itSiz_0]/2.)
        jm=int(vec_j0[itCell]-vec_cc[itSiz_0]/2.); jM=int(vec_j0[itCell]+vec_cc[itSiz_0]/2.)
        vec_i=range(im,iM)
        vec_j=range(jm,jM)
        mat_i,mat_j=np.meshgrid(vec_i,vec_j) 
        ze_X=np.empty([mat_i.size,2])
        ze_X[:,0]=np.reshape(mat_i,[mat_i.size])
        ze_X[:,1]=np.reshape(mat_j,[mat_j.size])
        if Lrg_Sml:
            ze_0=ze_X+[di_cur,dj_cur]
        else:
            ze_0=ze_X
        
        ze_1=ze_X+[di_cur,dj_cur]-vec_p0[itSiz_0,::-1]
        cell00=nd.map_coordinates(I0,ze_0.T,order=1) 
        cell01=nd.map_coordinates(I1,ze_1.T,order=1) 
        cell0=np.reshape(cell00,(int(sqrt(len(cell00))),int(sqrt(len(cell00))))).T
        cell1=np.reshape(cell01,(int(sqrt(len(cell00))),int(sqrt(len(cell00))))).T
        ##### Optimization of the measurement:
        ext_res=minimize(translation_correlation_weight,np.array([0.,0.]),args=(cell0,cell1,m_sht),method='Nelder-Mead',tol=1e-4,options={'maxiter':100,'maxfev':200,'disp':False},) 
        
        ####Storage of the results:
        share_vec[itCell]=di_cur-ext_res.x[0]-vec_p0[itSiz_0,1]
        share_vec[len(vec_i0)+itCell]=dj_cur-ext_res.x[1]-vec_p0[itSiz_0,0]
        share_vec[2*len(vec_i0)+itCell]=ext_res.fun
    
    else:
        share_vec[itCell]=di_cur
        share_vec[len(vec_i0)+itCell]=dj_cur
        share_vec[2*len(vec_i0)+itCell]=float('nan')
    
    return 0


########################
# Main displacement computation:

## Initialisation:
### To store displacements (line<->cell ; column<->picture):
mat_di=np.zeros([len(vec_i0),len(vec_img)+1])
mat_dj=np.zeros([len(vec_i0),len(vec_img)+1])
### To store correlation goodness (line<->cell ; column<->picture) [average error {0..255}]:
mat_corr=np.zeros([len(vec_i0),len(vec_img)+1])
### Load the initial picture:
I0=nd.imread(name_dir_im+'/'+name_im+str(nb_img_0).zfill(nb_dgt_im)+name_ext_im,flatten=1)
### Make Folder to save data:
system('mkdir resultDIC')

## Loop over the pictures: 
it_vec=0
for itImg in vec_img:
    
    ### Display:
    print('\n'+str(it_vec)+'/'+str(len(vec_img))+'\n')
    
    ### Load current picture:
    #### Loading:
    I1=nd.imread(name_dir_im+'/'+name_im+str(itImg+1).zfill(nb_dgt_im)+name_ext_im,flatten=1)
    
    ### Parallelisation over the correlation cells:
    p=Pool(nb_proc)
    p.map(main,range(len(vec_i0)))
    p.close()
    
    ### Storage of data:
    mat_di[:,it_vec+1]=share_vec[0:len(vec_i0)]
    mat_dj[:,it_vec+1]=share_vec[len(vec_i0):2*len(vec_i0)]
    mat_corr[:,it_vec+1]=share_vec[2*len(vec_i0):3*len(vec_i0)]
    
    ### Switch picture order:
    if Lrg_Sml:
        I0=I1.copy()
    
    del I1
    
    ### Graphical arrow output:
    if grph_3:
        ### Plot undeformed image:
        plt.imshow(image_tmp,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
        
        ### Plot displacements:
        for itCell in range(len(vec_i0)):
            i0=vec_i0[itCell]
            j0=vec_j0[itCell]
            di0=mat_di[itCell,it_vec+1]
            dj0=mat_dj[itCell,it_vec+1]
            plt.plot([j0,(j0+dj0)],[i0,(i0+di0)],'-r',linewidth=0.7)
        
        plt.title(str(itImg))
        plt.axis('off')
        plt.savefig('figure/tmp_arrow/'+str(it_vec).zfill(nb_dgt_im)+'.png',dpi=200)
        plt.close()
    
    it_vec+=1


### Save data:
np.savetxt('resultDIC/index_I.txt',vec_i0)
np.savetxt('resultDIC/index_J.txt',vec_j0)
np.savetxt('resultDIC/displacement_I.txt',mat_di)
np.savetxt('resultDIC/displacement_J.txt',mat_dj)
np.savetxt('resultDIC/correlation.txt',mat_corr)
np.savetxt('resultDIC/image_num.txt',vec_img)


########################
# Compute the strain tensor field:
if strain_cmpt:
    ## Make storage folder:
    system('mkdir resultDIC/strain_tensor')
    
    ## Load data:
    ### Cell positions:
    I0=np.loadtxt('resultDIC/index_I.txt')
    J0=np.loadtxt('resultDIC/index_J.txt')
    ### Cell displacements and correlation:
    mat_dI0=np.loadtxt('resultDIC/displacement_I.txt')
    mat_dJ0=np.loadtxt('resultDIC/displacement_J.txt')
    
    ## Coordinate vector:
    Iu0=np.sort(np.unique(I0))
    Ju0=np.sort(np.unique(J0))
    
    ## Make regular grid for coordinates:
    dIJ=int(dd_0/2.)
    vec_i=range(int(Iu0[0]),int(Iu0[-1]),dIJ)
    vec_j=range(int(Ju0[0]),int(Ju0[-1]),dIJ)
    grid_j,grid_i=np.meshgrid(vec_j,vec_i)
    vec_grid_i=grid_i.flatten()
    vec_grid_j=grid_j.flatten()
    
    ## Make storage variables:
    mat_grid_Eii=np.zeros((len(vec_grid_i),mat_dI0.shape[1]))
    mat_grid_Ejj=np.zeros((len(vec_grid_i),mat_dI0.shape[1]))
    mat_grid_Eij=np.zeros((len(vec_grid_i),mat_dI0.shape[1]))
    
    ## Loop over computed frames:
    for itImg in range(mat_dI0.shape[1]):
        ### Get current displacements:
        dI_cur=mat_dI0[:,itImg]
        dJ_cur=mat_dJ0[:,itImg]
        
        ### Extract fields by interpolation:
        grid_ui=griddata(np.array([I0,J0]).T,dI_cur,(grid_i,grid_j),method='linear')
        grid_uj=griddata(np.array([I0,J0]).T,dJ_cur,(grid_i,grid_j),method='linear')
        
        ### Compute strain tensor:
        grid_Eii=(grid_ui[1:grid_ui.shape[0],:]-grid_ui[0:grid_ui.shape[0]-1,:])/dIJ
        grid_Eii=np.append(grid_Eii,np.ones((1,grid_Eii.shape[1])),axis=0)
        grid_Eii[grid_Eii.shape[0]-1,:]=np.nan
        grid_Ejj=(grid_uj[:,1:grid_uj.shape[1]]-grid_uj[:,0:grid_uj.shape[1]-1])/dIJ
        grid_Ejj=np.append(grid_Ejj,np.ones((grid_Ejj.shape[0],1)),axis=1)
        grid_Ejj[:,grid_Ejj.shape[1]-1]=np.nan
        grid_Eij_a=(grid_ui[:,1:grid_ui.shape[1]]-grid_ui[:,0:grid_ui.shape[1]-1])/dIJ
        grid_Eij_a=np.append(grid_Eij_a,np.ones((grid_Eij_a.shape[0],1)),axis=1)
        grid_Eij_a[:,grid_Eij_a.shape[1]-1]=np.nan
        grid_Eij_b=(grid_uj[1:grid_uj.shape[0],:]-grid_uj[0:grid_uj.shape[0]-1,:])/dIJ
        grid_Eij_b=np.append(grid_Eij_b,np.ones((1,grid_Eij_b.shape[1])),axis=0)
        grid_Eij_b[grid_Eij_b.shape[0]-1,:]=np.nan
        grid_Eij=0.5*(grid_Eij_a+grid_Eij_b)
        
        ### Store it:
        mat_grid_Eii[:,itImg]=grid_Eii.flatten()
        mat_grid_Ejj[:,itImg]=grid_Ejj.flatten()
        mat_grid_Eij[:,itImg]=grid_Eij.flatten()
        
        # ~ #!Debug!:
        # ~ plt.imshow(grid_Eii)
        # ~ plt.show()
        # ~ plt.close()
    
    ## Store results:
    np.savetxt('resultDIC/strain_tensor/index_I.txt',vec_grid_i)
    np.savetxt('resultDIC/strain_tensor/index_J.txt',vec_grid_j)
    np.savetxt('resultDIC/strain_tensor/strain_Eii.txt',mat_grid_Eii)
    np.savetxt('resultDIC/strain_tensor/strain_Ejj.txt',mat_grid_Ejj)
    np.savetxt('resultDIC/strain_tensor/strain_Eij.txt',mat_grid_Eij)


########################
# Plot results:

## Make illustration movie:
if grph_3:
    system('ffmpeg -y -r  '+str(n_fps)+' -f image2 -i figure/tmp_arrow/%0'+str(nb_dgt_im)+'d.png -qscale 20 figure/displacement_arrow.avi')
    system('rm -rf figure/tmp_arrow')

## Make movie superimposing image and computed displacement intensity:
if grph_5:
    system('mkdir figure')
    system('mkdir figure/tmp_pict_1')
    system('mkdir figure/tmp_pict_2')
    if strain_cmpt:
        system('mkdir figure/tmp_pict_3')
    
    ## Load data:
    ### Cell positions:
    I0=np.loadtxt('resultDIC/index_I.txt')
    J0=np.loadtxt('resultDIC/index_J.txt')
    ### Cell displacements and correlation:
    mat_dI0=np.loadtxt('resultDIC/displacement_I.txt')
    mat_dJ0=np.loadtxt('resultDIC/displacement_J.txt')
    
    ## Coordinate vector:
    Iu0=np.sort(np.unique(I0))
    Ju0=np.sort(np.unique(J0))
    
    ## Make regular grid for coordinates:
    dIJ=int(dd_0/2.)
    vec_i=range(int(1.05*Iu0[0]),int(1.05*Iu0[-1]),dIJ)
    vec_j=range(int(1.05*Ju0[0]),int(1.05*Ju0[-1]),dIJ)
    grid_j,grid_i=np.meshgrid(vec_j,vec_i)
    
    ### Loop over the pictures: 
    it_vec=len(vec_img)
    for itImg in list(reversed(vec_img)):
        
        #### Load current picture:
        I0=nd.imread(name_dir_im+'/'+name_im+str(itImg).zfill(nb_dgt_im)+name_ext_im,flatten=1)
        
        #### Correlation cell positions:
        i0_cell=vec_i0+mat_di[:,it_vec]
        j0_cell=vec_j0+mat_dj[:,it_vec]
        d_cell=np.sqrt(mat_di[:,it_vec]**2.+mat_dj[:,it_vec]**2.)
        
        #### Plot displacement of the correlation cells:
        ##### Plot image:
        plt.imshow(I0,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
        ##### Plot the cell position:
        plt.plot(j0_cell,i0_cell,'bo',markersize=2)
        plt.title(str(itImg))
        plt.axis('off')
        plt.savefig('figure/tmp_pict_1/'+str(it_vec).zfill(nb_dgt_im)+'.png',dpi=200)
        plt.close()
        
        #### Plot displacement field:
        ##### Interpolate displacement field:
        grid_dcell=griddata(np.array([vec_i0,vec_j0]).T,d_cell,(grid_i,grid_j),method='linear')
        ##### Plot image:
        # ~ plt.imshow(I0,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
        ##### Plot field:
        if it_vec==len(vec_img):
            vec_test=grid_dcell.flatten()
            vec_test=np.sort(vec_test[np.where(~np.isnan(vec_test))[0]])
            d_cell_max=vec_test[int(0.95*len(vec_test))]
        
        plt.imshow(grid_dcell,vmin=0,vmax=d_cell_max,cmap=plt.cm.jet,extent=[np.min(grid_j),np.max(grid_j),np.min(grid_i),np.max(grid_i)],alpha=1., origin='lower')
        plt.colorbar()
        plt.axis('off')
        plt.axis('equal')
        plt.title(str(itImg))
        plt.savefig('figure/tmp_pict_2/'+str(it_vec).zfill(nb_dgt_im)+'.png',dpi=200)
        plt.close()
        
        if strain_cmpt:
            #### Plot von Mises strain field:
            ##### Compute the field:
            grid_Eii=np.reshape(mat_grid_Eii[:,it_vec],(grid_i.shape[0],grid_i.shape[1]))
            grid_Ejj=np.reshape(mat_grid_Ejj[:,it_vec],(grid_i.shape[0],grid_i.shape[1]))
            grid_Eij=np.reshape(mat_grid_Eij[:,it_vec],(grid_i.shape[0],grid_i.shape[1]))
            grid_VM=np.sqrt(grid_Eii**2.+grid_Ejj**2.-grid_Eii*grid_Ejj+3.*grid_Eij**2.)
            ##### Plot image:
            # ~ plt.imshow(I0,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
            ##### Plot field:
            if it_vec==len(vec_img):
                vec_test=grid_VM.flatten()
                vec_test=np.sort(vec_test[np.where(~np.isnan(vec_test))[0]])
                VM_min=vec_test[int(0.05*len(vec_test))]
                VM_max=vec_test[int(0.95*len(vec_test))]
            
            plt.imshow(grid_VM,vmin=VM_min,vmax=VM_max,cmap=plt.cm.jet,extent=[np.min(grid_j),np.max(grid_j),np.min(grid_i),np.max(grid_i)],alpha=1., origin='lower')
            plt.colorbar()
            plt.axis('off')
            plt.axis('equal')
            plt.title(str(itImg))
            plt.savefig('figure/tmp_pict_3/'+str(it_vec).zfill(nb_dgt_im)+'.png',dpi=200)
            plt.close()
        
        it_vec-=1
    
    ### Make movies:
    system('ffmpeg -y -r '+str(n_fps)+' -f image2 -i figure/tmp_pict_1/%0'+str(nb_dgt_im)+'d.png -qscale 20 figure/correlation_cell_position.avi')
    system('rm -rf figure/tmp_pict_1')
    system('ffmpeg -y -r '+str(n_fps)+' -f image2 -i figure/tmp_pict_2/%0'+str(nb_dgt_im)+'d.png -qscale 20 figure/displacement_magnitude.avi')
    system('rm -rf figure/tmp_pict_2')
    system('ffmpeg -y -r '+str(n_fps)+' -f image2 -i figure/tmp_pict_3/%0'+str(nb_dgt_im)+'d.png -qscale 20 figure/von_Mises_strain.avi')
    system('rm -rf figure/tmp_pict_3')


#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#~/:\~#
# Measure mechanical observables:

if E_cmpt:
    ## 2D strain deduced from DIC:
    vec_i0=np.array(vec_i0)
    vec_j0=np.array(vec_j0)
    ### Get areas where to measure displacements:
    Im=int(ym+0.05*(yM-ym)); IM=int(yM-0.05*(yM-ym))
    Jm_l=int(xm+0.01*(xM-xm)); JM_l=int(xm+0.2*(xM-xm))
    Jm_r=int(xM-0.2*(xM-xm)); JM_r=int(xM-0.01*(xM-xm))
    IJ_l=np.where((Im<vec_i0)*(vec_i0<IM)*(Jm_l<vec_j0)*(vec_j0<JM_l))[0]
    IJ_r=np.where((Im<vec_i0)*(vec_i0<IM)*(Jm_r<vec_j0)*(vec_j0<JM_r))[0]
    ### Measure strain:
    #### Get evolution of the boundaries:
    Il=np.mean(vec_i0[IJ_l])+np.mean(mat_di[IJ_l,:],axis=0)
    Jl=np.mean(vec_j0[IJ_l])+np.mean(mat_dj[IJ_l,:],axis=0)
    Ir=np.mean(vec_i0[IJ_r])+np.mean(mat_di[IJ_r,:],axis=0)
    Jr=np.mean(vec_j0[IJ_r])+np.mean(mat_dj[IJ_r,:],axis=0)
    
    #!Debug!:
    #~ for it_t in range(len(Il)):
        #~ image_tmp=nd.imread(name_dir_im+'/'+name_im+str(it_t).zfill(nb_dgt_im)+name_ext_im,flatten=1)
        #~ plt.imshow(image_tmp,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
        #~ plt.plot(vec_j0[IJ_l]+mat_dj[IJ_l,it_t],vec_i0[IJ_l]+mat_di[IJ_l,it_t],'.b')
        #~ plt.plot(vec_j0[IJ_r]+mat_dj[IJ_r,it_t],vec_i0[IJ_r]+mat_di[IJ_r,it_t],'.b')
        #~ plt.plot(Jl[it_t],Il[it_t],'r+')
        #~ plt.plot(Jr[it_t],Ir[it_t],'r+')
        #~ plt.show()
        #~ plt.close()
    
    #### Get strain:
    vec_eps_DIC=np.abs(np.sqrt((Il-Ir)**2.+(Jl-Jr)**2.)-np.sqrt((Il[0]-Ir[0])**2.+(Jl[0]-Jr[0])**2.))/np.sqrt((Il[0]-Ir[0])**2.+(Jl[0]-Jr[0])**2.)
    #### Save strain:
    np.savetxt('data/image_strain.txt',vec_eps_DIC)

if Y_cmpt:
    ## Get stress from loading device:
    vec_sig=np.abs(vec_f)/smpl_sec
    #### Save stress:
    np.savetxt('data/direct_stress.txt',vec_sig)
    
    ## Compute the Young modulus:
    ### Interpolate strain signal:
    vec_eps=np.interp(vec_t,vec_t_im[vec_img],vec_eps_DIC[1:])
    ### Save it:
    np.savetxt('data/direct_strain_interp.txt',vec_eps)
    ### Select and compute E:
    fig,ax=plt.subplots()
    plt.plot(vec_eps,vec_sig,'o-k')
    plt.xlabel('stain')
    plt.ylabel('stress (Pa)')
    ax.set_title('Select left and right edges for Young modulus fitting\n -right click to select, left click to zoom, close to end-')
    x0=[]; y0=[]
    cid=fig.canvas.mpl_connect('button_press_event', onclick)
    plt.show()
    plt.close()
    I_e=np.where((np.min(x0)<vec_eps)*(vec_eps<np.max(x0)))[0]
    if (np.max(np.abs(np.diff(I_e)))>1):
        I_e=I_e[0:np.where(np.diff(I_e)>1)[0][0]]
    
    p_E,V_E=np.polyfit(vec_eps[I_e],vec_sig[I_e],1,cov=True)
    if grph_6:
        system('mkdir figure')
        plt.plot(vec_eps,vec_sig,'o-k',markersize=4)
        plt.plot(vec_eps[I_e],p_E[0]*vec_eps[I_e]+p_E[1],'-r',linewidth=2)
        plt.xlabel('stain')
        plt.ylabel('stress (Pa)')
        plt.title('Young modulus: '+str(int(p_E[0]))+'Pa')
        plt.savefig('figure/strain_stress.png')
        plt.close()
    
    ###Save it:
    np.savetxt('data/Young_modulus.txt',np.array([abs(p_E[0])]))
    np.savetxt('data/Young_modulus_error.txt',np.array([np.sqrt(abs(V_E[0][0]))]))
    
    if N_cmpt:
        ## 2D Poisson ratio: 
        ### Get areas where to measure displacements:
        #### Along traction direction:
        Im=int(ym+0.05*(yM-ym)); IM=int(yM-0.05*(yM-ym))
        Jm_l=int(xm+0.01*(xM-xm)); JM_l=int(xm+0.2*(xM-xm))
        Jm_r=int(xM-0.2*(xM-xm)); JM_r=int(xM-0.01*(xM-xm))
        IJ_l=np.where((Im<vec_i0)*(vec_i0<IM)*(Jm_l<vec_j0)*(vec_j0<JM_l))[0]
        IJ_r=np.where((Im<vec_i0)*(vec_i0<IM)*(Jm_r<vec_j0)*(vec_j0<JM_r))[0]
        Il=np.mean(vec_i0[IJ_l])+np.mean(mat_di[IJ_l,:],axis=0)
        Jl=np.mean(vec_j0[IJ_l])+np.mean(mat_dj[IJ_l,:],axis=0)
        Ir=np.mean(vec_i0[IJ_r])+np.mean(mat_di[IJ_r,:],axis=0)
        Jr=np.mean(vec_j0[IJ_r])+np.mean(mat_dj[IJ_r,:],axis=0)
        #### Normal to traction direction:
        Jm=int(xm+0.15*(xM-xm)); JM=int(xM-0.15*(xM-xm))
        Im_t=int(ym+0.05*(yM-ym)); IM_t=int(ym+0.25*(yM-ym))
        Im_d=int(yM-0.25*(yM-ym)); IM_d=int(yM-0.05*(yM-ym))
        IJ_t=np.where((Im_t<vec_i0)*(vec_i0<IM_t)*(Jm<vec_j0)*(vec_j0<JM))[0]
        IJ_d=np.where((Im_d<vec_i0)*(vec_i0<IM_d)*(Jm<vec_j0)*(vec_j0<JM))[0]
        It=np.mean(vec_i0[IJ_t])+np.mean(mat_di[IJ_t,:],axis=0)
        Jt=np.mean(vec_j0[IJ_t])+np.mean(mat_dj[IJ_t,:],axis=0)
        Id=np.mean(vec_i0[IJ_d])+np.mean(mat_di[IJ_d,:],axis=0)
        Jd=np.mean(vec_j0[IJ_d])+np.mean(mat_dj[IJ_d,:],axis=0)
        
        #~ #!Debug!:
        #~ for it_t in range(len(Il)):
            #~ image_tmp=nd.imread(name_dir_im+'/'+name_im+str(it_t).zfill(nb_dgt_im)+name_ext_im,flatten=1)
            #~ plt.imshow(image_tmp,cmap=plt.cm.Greys,vmin=0,vmax=3.*np.mean(image_tmp))
            #~ plt.plot(vec_j0[IJ_l]+mat_dj[IJ_l,it_t],vec_i0[IJ_l]+mat_di[IJ_l,it_t],'.b')
            #~ plt.plot(vec_j0[IJ_r]+mat_dj[IJ_r,it_t],vec_i0[IJ_r]+mat_di[IJ_r,it_t],'.b')
            #~ plt.plot(vec_j0[IJ_t]+mat_dj[IJ_t,it_t],vec_i0[IJ_t]+mat_di[IJ_t,it_t],'.g')
            #~ plt.plot(vec_j0[IJ_d]+mat_dj[IJ_d,it_t],vec_i0[IJ_d]+mat_di[IJ_d,it_t],'.g')
            #~ plt.plot(Jl[it_t],Il[it_t],'r+')
            #~ plt.plot(Jr[it_t],Ir[it_t],'r+')
            #~ plt.plot(Jt[it_t],It[it_t],'r+')
            #~ plt.plot(Jd[it_t],Id[it_t],'r+')
            #~ plt.show()
            #~ plt.close()
        
        ### Compute the evolution of the Poisson ratio:
        L0=np.sqrt((Il[0]-Ir[0])**2.+(Jl[0]-Jr[0])**2.)
        L1=np.sqrt((Il-Ir)**2.+(Jl-Jr)**2.)
        l0=np.sqrt((It[0]-Id[0])**2.+(Jt[0]-Jd[0])**2.)
        l1=np.sqrt((It-Id)**2.+(Jt-Jd)**2.)
        vec_nu_DIC=((l0-l1)/l0)/((L1-L0)/L0)
        J=np.where(~np.isnan(vec_nu_DIC))
        vec_nu_DIC=vec_nu_DIC[J]
        ### Interpolate the signal:
        vec_nu=np.interp(vec_t,vec_t_im[vec_img[0:len(vec_nu_DIC)]],vec_nu_DIC)
        ### Save it:
        np.savetxt('data/direct_Poisson_interp.txt',vec_nu)
        ### Select and compute E:
        fig,ax=plt.subplots()
        plt.plot(vec_eps,vec_nu,'o-k')
        plt.plot(vec_eps[I_e],vec_nu[I_e],'.b')
        plt.xlabel('strain')
        plt.ylabel('Poisson ratio')
        ax.set_title('Select left and right edges for averaging\n -right click to select, left click to zoom, close to end-')
        x0=[]; y0=[]
        cid=fig.canvas.mpl_connect('button_press_event', onclick)
        plt.show()
        plt.close()
        I_n=np.where((np.min(x0)<vec_eps)*(vec_eps<np.max(x0)))[0]
        if (np.max(np.abs(np.diff(I_n)))>1):
            I_n=I_n[0:np.where(np.diff(I_n)>1)[0][0]]
        
        nu0=np.mean(vec_nu[I_n])
        err_nu0=np.std(vec_nu[I_n])/sqrt(len(I_n))
        if grph_6:
            plt.plot(vec_eps,vec_nu,'o-k',markersize=4)
            plt.plot(np.array([vec_eps[I_n[0]],vec_eps[I_n[-1]]]),nu0*np.ones(2),'-r',linewidth=2)
            plt.xlim(np.min(vec_eps),np.max(vec_eps))
            plt.ylim(nu0-1,nu0+1)
            plt.xlabel('stain')
            plt.ylabel('Poisson ratio')
            plt.title('Poisson ratio: '+str(nu0))
            plt.savefig('figure/strain_Poisson.png')
            plt.close()
        
        ###Save it:
        np.savetxt('data/Poisson_ratio.txt',np.array([nu0]))
        np.savetxt('data/Poisson_ratio_error.txt',np.array([err_nu0]))
    
    if PM_cmpt:
        ## Computation of the plastic modulus:
        ### Select and compute plastic modulus:
        fig,ax=plt.subplots()
        plt.plot(vec_eps,vec_sig,'o-k')
        plt.xlabel('stain')
        plt.ylabel('stress (Pa)')
        ax.set_title('Select left and right edges for plastic modulus fitting\n -right click to select, left click to zoom, close to end-')
        x0=[]; y0=[]
        cid=fig.canvas.mpl_connect('button_press_event', onclick)
        plt.show()
        plt.close()
        I_p=np.where((np.min(x0)<vec_eps)*(vec_eps<np.max(x0)))[0]
        if (np.max(np.abs(np.diff(I_p)))>1):
            I_p=I_p[0:np.where(np.diff(I_p)>1)[0][0]]
        
        p_PM,V_PM=np.polyfit(vec_eps[I_p],vec_sig[I_p],1,cov=True)
        if grph_6:
            system('mkdir figure')
            plt.plot(vec_eps,vec_sig,'o-k',markersize=4)
            plt.plot(vec_eps[I_e],p_E[0]*vec_eps[I_e]+p_E[1],'-r',linewidth=2)
            plt.plot(vec_eps[I_p],p_PM[0]*vec_eps[I_p]+p_PM[1],'-b',linewidth=2)
            plt.xlabel('stain')
            plt.ylabel('stress (Pa)')
            plt.title('Young modulus: '+str(int(p_E[0]))+'Pa - Plastic modulus: '+str(int(p_PM[0]))+'Pa')
            plt.savefig('figure/strain_stress.png')
            plt.close()
        
        ###Save it:
        np.savetxt('data/Plastic_modulus.txt',np.array([abs(p_PM[0])]))
        np.savetxt('data/Plastic_modulus_error.txt',np.array([np.sqrt(abs(V_PM[0][0]))]))
    
    if sigM_cmpt:
        ###Computation of the maximum strain:
        I_m=np.where(vec_sig==np.max(vec_sig))[0][0]
        sig_m=vec_sig[I_m]
        eps_m=vec_eps[I_m]
        if grph_6:
            system('mkdir figure')
            plt.plot(vec_eps,vec_sig,'o-k',markersize=4)
            plt.plot(vec_eps[I_e],p_E[0]*vec_eps[I_e]+p_E[1],'-r',linewidth=2)
            plt.plot(vec_eps[I_p],p_PM[0]*vec_eps[I_p]+p_PM[1],'-b',linewidth=2)
            plt.plot([np.min(vec_eps),eps_m],sig_m*np.ones(2),'-m',linewidth=2)
            plt.plot(eps_m*np.ones(2),[np.min(vec_sig),sig_m],'-m',linewidth=2)
            plt.xlabel('stain')
            plt.ylabel('stress (Pa)')
            plt.title('Young modulus: '+str(int(p_E[0]))+'Pa - Plastic modulus: '+str(int(p_PM[0]))+'Pa')
            plt.savefig('figure/strain_stress.png')
            plt.close()
        
        ###Save it:
        np.savetxt('data/Maximum_stress.txt',np.array([sig_m]))
        np.savetxt('data/Strain_at_maximum_stress.txt',np.array([eps_m]))
    
    if sigT_cmpt:
        ###Compute he transition point between elastic and plastic behavior:
        eps_t=(p_PM[1]-p_E[1])/(p_E[0]-p_PM[0])
        sig_t=p_E[0]*eps_t+p_E[1]
        if grph_6:
            system('mkdir figure')
            plt.plot(vec_eps,vec_sig,'o-k',markersize=4)
            plt.plot(np.array([np.min(vec_eps[I_e]),eps_t]),p_E[0]*np.array([np.min(vec_eps[I_e]),eps_t])+p_E[1],'-r',linewidth=2)
            plt.plot(np.array([eps_t,np.max(vec_eps[I_p])]),p_PM[0]*np.array([eps_t,np.max(vec_eps[I_p])])+p_PM[1],'-b',linewidth=2)
            plt.plot([np.min(vec_eps),eps_m],sig_m*np.ones(2),'-m',linewidth=2)
            plt.plot(eps_m*np.ones(2),[np.min(vec_sig),sig_m],'-m',linewidth=2)
            plt.plot([np.min(vec_eps),eps_t],sig_t*np.ones(2),'-c',linewidth=2)
            plt.plot(eps_t*np.ones(2),[np.min(vec_sig),sig_t],'-c',linewidth=2)
            plt.xlabel('stain')
            plt.ylabel('stress (Pa)')
            plt.title('Young modulus: '+str(int(p_E[0]))+'Pa - Plastic modulus: '+str(int(p_PM[0]))+'Pa')
            plt.savefig('figure/strain_stress.png')
            plt.close()
        
        ###Save it:
        np.savetxt('data/Plastic_stress.txt',np.array([sig_t]))
        np.savetxt('data/Strain_at_plastic_stress.txt',np.array([eps_t]))
    
    if sigL_cmpt:
        ###Computation of the stress before breaking:
        d_sig=-np.diff(vec_sig)
        v_test=np.mean(d_sig)+1.*np.std(d_sig)
        I_l=np.where(d_sig>v_test)[0][0]
        sig_l=vec_sig[I_l]
        eps_l=vec_eps[I_l]
        
        if grph_6:
            system('mkdir figure')
            plt.plot(vec_eps,vec_sig,'o-k',markersize=4)
            plt.plot(np.array([np.min(vec_eps[I_e]),eps_t]),p_E[0]*np.array([np.min(vec_eps[I_e]),eps_t])+p_E[1],'-r',linewidth=2)
            plt.plot(np.array([eps_t,np.max(vec_eps[I_p])]),p_PM[0]*np.array([eps_t,np.max(vec_eps[I_p])])+p_PM[1],'-b',linewidth=2)
            plt.plot([np.min(vec_eps),eps_m],sig_m*np.ones(2),'-m',linewidth=2)
            plt.plot(eps_m*np.ones(2),[np.min(vec_sig),sig_m],'-m',linewidth=2)
            plt.plot([np.min(vec_eps),eps_t],sig_t*np.ones(2),'-c',linewidth=2)
            plt.plot(eps_t*np.ones(2),[np.min(vec_sig),sig_t],'-c',linewidth=2)
            plt.plot([np.min(vec_eps),eps_l],sig_l*np.ones(2),'-y',linewidth=2)
            plt.plot(eps_l*np.ones(2),[np.min(vec_sig),sig_l],'-y',linewidth=2)
            plt.xlabel('stain')
            plt.ylabel('stress (Pa)')
            plt.title('Young modulus: '+str(int(p_E[0]))+'Pa - Plastic modulus: '+str(int(p_PM[0]))+'Pa')
            plt.savefig('figure/strain_stress.png')
            plt.close()
        
        ###Save it:
        np.savetxt('data/Breaking_stress.txt',np.array([sig_l]))
        np.savetxt('data/Strain_at_breaking_stress.txt',np.array([eps_l]))
